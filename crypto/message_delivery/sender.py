from abc import ABC, abstractmethod

class Sender(ABC):
    @abstractmethod
    def send(self, message):
        pass

class MockSender(Sender):
    def send(self, message):
        print(f"\nSent using mock sender: {message}")


#Below is for testing this .py file ONLY, not to be run with main.py
#x = MockSender
#x.send("message=", "How are you? 19")
















#x = AnotherSubclass()
#x.do_something()

#########################################
"""
class AbstractClassExample(ABC):
    @abstractmethod
    def do_something(self):
        print("Some implementation!")
        
class AnotherSubclass(AbstractClassExample):
    def do_something(self):
        super().do_something()
        print("The enrichment from AnotherSubclass")

x = AnotherSubclass()
x.do_something()
"""