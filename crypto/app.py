from time import sleep

class App:
    def __init__(self, events, time_interval):
        self.events = events
        self.time_interval = time_interval
    
    def start(self):
        while True:
            for event in self.events:
                event.check()
            sleep(self.time_interval)
            



