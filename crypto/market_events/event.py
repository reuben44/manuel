from abc import ABC, abstractmethod

class Event(ABC):
    def __init__(self, senders):
        self.senders = senders

    @property
    @abstractmethod
    def condition(self):
        pass

    @property
    @abstractmethod
    def message(self):
        pass

    def check(self):
        if self.condition:
            for sender in self.senders:
                sender.send(self.message)

class MockEvent(Event):
    @property
    def condition(self):
        return True
        
    @property
    def message(self):
        message = "\n A mock message to describe a mock event..."
        return message





